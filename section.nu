# the current directory
export def dir [] {$'(ansi c)($env.YASP_DIR) '}

# the current git branch
export def git_branch [] {
	if ($env.PWD | list_pardirs | any {|i| $"($i)/.git" | path exists}) {
		let res = (do -i { ^git branch --show-current | decode utf-8 })
		if $res != '' {$'(ansi lp)($res | str trim) '}
	}
}

# the last exit code (if it was != 0)
export def exit_code [] {
	if $env.LAST_EXIT_CODE != 0 {$'(ansi lrb)✖($env.LAST_EXIT_CODE) '}
}

# active nushell overlays
export def nu_overlay [
	hide = ['zero' 'packer_api' 'packer_packages' 'conditional_packages']  # a list of names to exclude
] {
	let list = (overlay list | where not ($it in $hide) | str join ',')
	if $list != '' {$'(ansi y)($list) '}
}

# show a icon if a pyvenv from the pyvenv package is active
export def pyvenv [] {
	if ($env | get -i PYTHON_VENVS.0) != null {return ' '}
	''
}

# from https://github.com/jan9103/nutils, but embedded since packer.nu does not support libraries
def list_pardirs []: string -> list<string> {
  let parts = ($in | path split)
  0..(($parts | length) - 1)
  | each {|i| $parts | rng 0..($i) | path join}
}

def rng [a] {
	let v = (version)
	if ($v.minor > 101) or ($v.minor == 101 and $v.patch > 0) {
		$in | slice $a
	} else {
		$in | range $a
	}
}
